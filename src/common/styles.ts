import styled from 'styled-components';

export const FlexDiv = styled.div`
    display: flex;
`;

export const FullSizeFlexDiv = styled(FlexDiv)`
    height: 100%;
    width: 100%;
`;
export const FlexCenter = styled(FlexDiv)`
    justify-content: center;
    align-items: center;
`;

export const FlexAlignLeft = styled(FlexDiv)`
    justify-content: flex-start;
    align-items: center;
`;

export const FlexAlignRight = styled(FlexDiv)`
    justify-content: flex-end;
    align-items: center;
`;

export const FullSizeFlexCenteredDiv = styled(FullSizeFlexDiv)`
    justify-content: center;
    align-items: center;
`;

export const FullSizeFlexAlignLeft = styled(FullSizeFlexDiv)`
    justify-content: flex-start;
    align-items: center;
`;

export const FullSizeFlexAlignRight = styled(FullSizeFlexDiv)`
    justify-content: flex-end;
    align-items: center;
`;
