import * as jsonp from 'jsonp';
import { promisify } from 'es6-promisify';
import { parseRecipesFromAPI } from './utils';

const promiseJSONP = promisify(jsonp);
const recipeRequestHandler =
    (ingredients: string, search: string, page: number) =>
        promiseJSONP(`http://www.recipepuppy.com/api/?i=${ingredients}&q=${search}&p=${page}`, { name: 'callbackResponse' });


export const cacheHashGenerator = (ingredients: string, searchText: string): string => ingredients + searchText;

type SearchStructure = {
    reachedLimit: boolean;
    recipesPerPage: Recipe[][];
}

export class APICache {

    public static getInstance = () => {
        if (APICache.instance === null) {
            APICache.instance = new APICache();
        }
        return APICache.instance;
    }

    private static instance: APICache | null = null;
    private registers: Map<string, SearchStructure>;

    private constructor() {
        this.registers = new Map();
    }

    public requestRecipes = async (ingredients: string, searchText: string, loadMore: boolean): Promise<Recipe[]> => {
        const hash = cacheHashGenerator(ingredients, searchText);
        let recipeList: SearchStructure | undefined = this.registers.get(hash);
        if (recipeList) {
            if (!loadMore) return recipeList.recipesPerPage.reduce((total, current) => total.concat(current), []);
        } else {
            recipeList = {
                reachedLimit: false,
                recipesPerPage: []
            };
        }
        if (!recipeList.reachedLimit) {
            const page = recipeList.recipesPerPage.length + 1;
            const response = await recipeRequestHandler(ingredients, searchText, page);
            if (response.error) {
                throw (response.error.message)
            }
            const recipesResponse = parseRecipesFromAPI(response.results);
            if (recipesResponse.length) {
                recipeList.recipesPerPage[page - 1] = recipesResponse;
            }
            recipeList.reachedLimit = (recipesResponse.length === 0);
            this.registers.set(hash, recipeList);
        }
        return recipeList.recipesPerPage.reduce((total, current) => total.concat(current), []);;
    }
}