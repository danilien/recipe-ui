export enum ServiceRequestStatus {
    INACTIVE = 'INACTIVE',
    PENDING = 'PENDING',
    RECEIVED = 'RECEIVED',
    ERROR = 'ERROR'
}

export const CONNECTION_ERROR = 'An error ocurred, please check your connection and refresh the page';