import { XmlEntities } from 'html-entities';

const entities = new XmlEntities();

const lactoseIngredientes: string[] = ['milk', 'cheese'];

export const containsLactose = (ingredients: string) => {
    for (let i = 0; i < lactoseIngredientes.length; i++) {
        if (ingredients.includes(lactoseIngredientes[i])) return true;
    }
    return false;
}

export const parseRecipesFromAPI = (rawRecipeList: any[]): Recipe[] =>
    rawRecipeList.map(rawRecipe => {
        const ingredients: string[] = rawRecipe.ingredients.split(',').map((ingredient: string) => entities.decode(ingredient.trim()));
        return {
            title: entities.decode(rawRecipe.title),
            href: rawRecipe.href,
            ingredients,
            thumbnail: rawRecipe.thumbnail,
            containsLactose: containsLactose(rawRecipe.ingredients)
        };
    })