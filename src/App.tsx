import * as React from 'react';
import { Provider } from 'react-redux';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import store from './state/store'
import { AppContainer, AppContent } from './styles';
import RecipeList from './features/recipeList';
import { positions, Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

const alertOptions = {
    timeout: 5000,
    position: positions.CENTER
};

const history = createBrowserHistory();

class App extends React.Component<{}, {}> {
    constructor(props: {}) {
        super(props);
    }

    public render() {
        return (
            <Provider store={store}>
                <Router history={history}>
                    <AlertProvider template={AlertTemplate} {...alertOptions}>
                        <AppContainer>
                            <AppContent>
                                <Switch>
                                    <Route path={`/recipeList`} component={RecipeList} />
                                    <Redirect to="/recipeList" />
                                </Switch>
                            </AppContent>
                        </AppContainer>
                    </AlertProvider>
                </Router>
            </Provider>
        );
    }
}

export default App;
