import { combineReducers } from "redux";
import { RootState } from "./types";
import recipeListReducer from 'src/features/recipeList/state/reducer';

const rootReducer = combineReducers<RootState>({
    recipeListState: recipeListReducer
});

export default rootReducer;
