import { RecipeListState } from 'src/features/RecipeList/State/reducer';

export interface RootState {
    recipeListState: RecipeListState
}
