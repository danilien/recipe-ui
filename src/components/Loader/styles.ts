
import styled, { css, keyframes } from 'styled-components';

export const Spinner = styled.div`
    z-index: 2;
    position: fixed;
    top: 0;
    left: 0;
    width:100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.3);
`

export const skbounce = keyframes` 
    0%, 100% { 
      transform: scale(0.0);
    } 50% { 
      transform: scale(1.0);
    }
`
export const bounce = css`
    ${skbounce} 2.0s infinite ease-in-out;
`

export const DoubleBounce1 = styled.div`
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background-color: #373F53;
    opacity: 0.6;
    position: fixed;
    top: calc(50% - 25px);
    left: calc(50% - 25px);
    animation: ${bounce};
`;

export const DoubleBounce2 = styled(DoubleBounce1)`
    animation-delay: -1.0s;
`

