import * as React from 'react';
import {
    DoubleBounce1,
    DoubleBounce2,
    Spinner
} from './styles';

export class Loader extends React.PureComponent {
    constructor(props: {}) {
        super(props);
    }

    public render() {
        return (
            <Spinner>
                <DoubleBounce1 />
                <DoubleBounce2 />
            </Spinner>
        );
    }
};

export default Loader;
