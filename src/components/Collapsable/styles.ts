import styled from 'styled-components';
import { FullSizeFlexDiv } from 'src/common/styles';

export const Container = styled.div`
`

export const Header = styled(FullSizeFlexDiv)`
    padding: 10px;
    transition-duration: 0.2s;
    transition-timing-function: ease-in;
    font-size: 20px;
    ${(props: { isOpen: boolean }) => props.isOpen && 'transform: rotate(90deg);'}
`

export const Content = styled.div`
    width: 50%;
    position: fixed;
    background-color: white;
    color: black;
    border-left: solid 1px #f2f2f2;
    border-right: solid 1px #f2f2f2;
    border-bottom: solid 1px #f2f2f2;
    border-radius: 0 0 5px 5px;
    font-family: verdana;
    font-size: 14px;
    @media (max-width: 768px) {
        width: 80%;
    }
`