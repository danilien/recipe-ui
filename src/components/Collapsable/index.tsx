import * as React from 'react';
import { Header, Content, Container } from './styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'

export namespace Collapsable {
    export interface State { open: boolean; }
}

export const elementIdCollection = {
    headerId: 'headerId',
    contentId: 'contentId'
}

export class Collapsable extends React.Component<{}, Collapsable.State> {

    private node;

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    handleOutsideClick = (e: any) => {
        // ignore clicks on the component itself
        if (this.node) {
            if (this.node.contains(e.target)) return;
            this.handleClick();
        }
    }

    handleClick = () => {
        if (!this.state.open) {
            document.addEventListener('click', this.handleOutsideClick, false);
        } else {
            document.removeEventListener('click', this.handleOutsideClick, false);
        }
        this.setState(prevState => ({
            open: !prevState.open,
        }));
    }

    render() {
        return (
            <Container ref={node => { this.node = node; }}>
                <Header
                    id={elementIdCollection.headerId}
                    onClick={() => this.handleClick()}
                    isOpen={this.state.open}
                >
                    <FontAwesomeIcon icon={faAngleRight} />
                </Header>
                {
                    this.state.open &&
                    <Content id={elementIdCollection.contentId}>
                        {this.props.children}
                    </Content>
                }
            </Container>
        );
    }
}