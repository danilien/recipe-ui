import recipeListReducer, { RecipeListState } from 'src/features/recipeList/state/reducer';
import {
    ActionRequestRecipes,
    REQUEST_RECIPES,
    REQUEST_RECIPES_SUCCESS,
    ActionRequestRecipesSuccess,
    ActionRequestRecipesFail,
    REQUEST_RECIPES_FAILURE
} from 'src/features/recipeList/state/actions';
import { ServiceRequestStatus } from 'src/common/constants';
import { mockParsedRecipeList } from 'src/__tests__/mockedRecipeData';

const mockedState1: RecipeListState = {
    requestStatus: ServiceRequestStatus.PENDING,
    recipeList: [],
    errorMessage: null
};

const mockedState2: RecipeListState = {
    requestStatus: ServiceRequestStatus.RECEIVED,
    recipeList: mockParsedRecipeList,
    errorMessage: null
};

const mockedState3: RecipeListState = {
    requestStatus: ServiceRequestStatus.ERROR,
    recipeList: [],
    errorMessage: 'UnknownError'
};

describe("recipeList Reducer", () => {
    test("request recipes action", () => {
        const requestRecipeAction: ActionRequestRecipes = {
            type: REQUEST_RECIPES,
            searchText: 'searchText',
            loadMore: true
        };
        expect(recipeListReducer(undefined, requestRecipeAction)).toEqual(mockedState1);
    });

    test("request recipes success action", () => {
        const requestRecipeActionSuccess: ActionRequestRecipesSuccess = {
            type: REQUEST_RECIPES_SUCCESS,
            recipeList: mockParsedRecipeList
        };
        expect(recipeListReducer(mockedState1, requestRecipeActionSuccess)).toEqual(mockedState2);
    });

    test("request recipes error action", () => {
        const requestRecipeActionFail: ActionRequestRecipesFail = {
            type: REQUEST_RECIPES_FAILURE,
            error: 'UnknownError'
        };
        expect(recipeListReducer(mockedState1, requestRecipeActionFail)).toEqual(mockedState3);
    });
});