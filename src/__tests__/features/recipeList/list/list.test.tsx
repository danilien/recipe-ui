import * as React from 'react';
import { shallow } from 'enzyme'
import List from 'src/features/recipeList/list';
import { mockParsedRecipeList } from '../../../mockedRecipeData';
import RecipeItem from 'src/features/recipeList/recipeItem';
import toJson from 'enzyme-to-json';

const mockedProps = {
    recipeList: mockParsedRecipeList,
    onViewDetailClick: jest.fn(),
    loadMoreRecipes: jest.fn()
}

describe('recipeList-recipeItem component', () => {
    test('renders without crashing', () => {
        const wrapper = shallow(<List {...mockedProps} />)
        expect(wrapper).toMatchSnapshot()
    })

    test('count RenderItem elements', () => {
        const wrapper = shallow(<List {...mockedProps} />)
        expect(wrapper.find(RecipeItem).length).toBe(mockParsedRecipeList.length);
    })

    test('call loadMoreRecipes on scroll', () => {
        const wrapper = shallow(<List {...mockedProps} />)
        wrapper.simulate('scroll', { target: { scrollHeight: 100, scrollTop: 200, clientHeight: 200 } })
        expect(mockedProps.loadMoreRecipes).toBeCalled();
    })
})