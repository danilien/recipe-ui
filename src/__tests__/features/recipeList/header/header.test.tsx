import * as React from 'react';
import { shallow, mount } from 'enzyme'
import { Header, elementIdCollection } from 'src/features/recipeList/header';

const mockedProps = {
    searchText: 'someRecipeName',
    onChangeSearchText: jest.fn(),
    forceSearch: jest.fn(),
    historySearchList: [],
    searchAgain: jest.fn(),
    deleteSearch: jest.fn()
}

describe('recipeList-header component', () => {
    test('renders without crashing', () => {
        const wrapper = shallow(<Header {...mockedProps} />)
        expect(wrapper).toMatchSnapshot()
    })

    test('search text onChange', () => {
        const wrapper = shallow(<Header {...mockedProps} />)
        const input = wrapper.find(`#${elementIdCollection.inputId}`);
        input.simulate('change', { target: { value: 'Hello' } })
        expect(mockedProps.onChangeSearchText).toBeCalled();
    })

    test('keypress on input Enter/no Enter', () => {
        const wrapper = shallow(<Header {...mockedProps} />)
        const input = wrapper.find(`#${elementIdCollection.inputId}`);
        input.simulate('keypress', { key: 'u' })
        expect(mockedProps.forceSearch).not.toBeCalled();
        input.simulate('keypress', { key: 'Enter' })
        expect(mockedProps.forceSearch).toBeCalled();
    })

    test('submit button click', () => {
        const wrapper = shallow(<Header {...mockedProps} />)
        const submit = wrapper.find(`#${elementIdCollection.submitId}`);
        submit.simulate('click')
        expect(mockedProps.forceSearch).toBeCalled();
    })
})