import * as React from 'react';
import { shallow } from 'enzyme'
import SearchMenuHistory, { elementIdCollection } from 'src/features/recipeList/header/searchHistoryMenu';

const mockedProps1 = {
    historySearchList: ['someRecipeName'],
    searchAgain: jest.fn(),
    deleteSearch: jest.fn()
}

const mockedProps2 = {
    ...mockedProps1,
    searchAgain: jest.fn(),
    deleteSearch: jest.fn()
}

const mockedProps3 = {
    ...mockedProps1,
    searchAgain: jest.fn(),
    deleteSearch: jest.fn()
}

describe('recipeList-header-searchHistoryMenu component', () => {
    test('renders without crashing', () => {
        const wrapper = shallow(<SearchMenuHistory {...mockedProps1} />)
        expect(wrapper).toMatchSnapshot()
    })

    test('click search option', () => {
        const wrapper = shallow(<SearchMenuHistory {...mockedProps2} />)
        const element = wrapper.find(`#${elementIdCollection.searchText(0)}`);
        element.simulate('click')
        expect(mockedProps2.searchAgain).toBeCalled();
        expect(mockedProps2.deleteSearch).not.toBeCalled();
    })

    test('click delete option', () => {
        const wrapper = shallow(<SearchMenuHistory {...mockedProps3} />)
        const element = wrapper.find(`#${elementIdCollection.delete(0)}`);
        element.simulate('click')
        expect(mockedProps3.searchAgain).not.toBeCalled();
        expect(mockedProps3.deleteSearch).toBeCalled();
    })
})