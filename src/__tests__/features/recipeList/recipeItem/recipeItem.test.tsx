import * as React from 'react';
import { shallow } from 'enzyme'
import RecipeItem, { elementIdCollection } from 'src/features/recipeList/recipeItem';

const mockedProps = {
    viewDetailClick: jest.fn(),
    recipe: {
        title: 'someRecipeName',
        href: '',
        ingredients: ['milk', 'chocolate', 'sugar'],
        thumbnail: '',
        containsLactose: true
    }
}

describe('recipeList-recipeItem component', () => {
    test('renders without crashing', () => {
        const wrapper = shallow(<RecipeItem {...mockedProps} />)
        expect(wrapper).toMatchSnapshot()
    })

    test('click viewdetail', () => {
        const wrapper = shallow(<RecipeItem {...mockedProps} />)
        const container = wrapper.find(`#${elementIdCollection.containerId}`);
        container.simulate('click')
        expect(mockedProps.viewDetailClick).toBeCalled();
    })

    test('render all ingredientes', () => {
        const wrapper = shallow(<RecipeItem {...mockedProps} />)
        mockedProps.recipe.ingredients.forEach((ingredient, i) => {
            const element = wrapper.find(`#${elementIdCollection.ingredientId(i)}`);
            expect(element.children().text()).toBe(ingredient);
        })
    })
})