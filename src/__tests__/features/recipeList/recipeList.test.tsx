import * as React from 'react';
import { shallow } from 'enzyme'
import { RecipeList } from 'src/features/recipeList';
import { ServiceRequestStatus } from 'src/common/constants';
import { mockParsedRecipeList } from '../../mockedRecipeData';
import List from 'src/features/recipeList/list';
import Header from 'src/features/recipeList/header';

const mockedProps = {
    requestRecipes: jest.fn(),
    requestStatus: ServiceRequestStatus.RECEIVED,
    errorMessage: null,
    recipeList: mockParsedRecipeList
}

describe('recipeList tests', () => {
    describe('no-connected component', () => {
        test('renders without crashing', () => {
            const wrapper = shallow(<RecipeList {...mockedProps} />)
            expect(wrapper).toMatchSnapshot()
        })

        test('renders List and Header', () => {
            const wrapper = shallow(<RecipeList {...mockedProps} />)
            expect(wrapper.find(List)).toBeDefined();
            expect(wrapper.find(Header)).toBeDefined();
        })
    })
})