import { parseRecipesFromAPI, containsLactose } from 'src/common/utils';
import { mockRawRecipeList, mockParsedRecipeList } from '../mockedRecipeData';

describe('utils functions test', () => {
    test('parseRecipesFromAPI test', () => {
        const parsedStringResult = JSON.stringify(parseRecipesFromAPI(mockRawRecipeList));
        const expectedStringResult = JSON.stringify(mockParsedRecipeList);
        expect(parsedStringResult).toMatch(expectedStringResult);
    })

    test('containsLactose test', () => {
        expect(containsLactose('')).toBeFalsy();
        expect(containsLactose('milk, chocolate, onion')).toBeTruthy();
        expect(containsLactose('chocolate, onion')).toBeFalsy();
    })
})