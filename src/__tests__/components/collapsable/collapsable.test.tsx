import * as React from 'react';
import { shallow } from 'enzyme'
import { Collapsable, elementIdCollection } from 'src/components/Collapsable';

describe('collapsable component', () => {
    test('renders without crashing', () => {
        const wrapper = shallow(<Collapsable />)
        expect(wrapper).toMatchSnapshot()
    })

    test('test open/close click menu', () => {
        const wrapper = shallow(<Collapsable />)
        const header = wrapper.find(`#${elementIdCollection.headerId}`);
        const noContent = wrapper.find(`#${elementIdCollection.contentId}`);
        expect(noContent.exists()).toBeFalsy();
        header.simulate('click');
        const content = wrapper.find(`#${elementIdCollection.contentId}`);
        expect(content.exists()).toBeTruthy();
    })
})