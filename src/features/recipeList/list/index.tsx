import * as React from 'react';
import { Container } from './styles';
import RecipeItem from '../recipeItem';

export namespace List {
    export interface Props {
        recipeList: Recipe[];
        onViewDetailClick: (recipe: Recipe) => void;
        loadMoreRecipes: () => void;
    }
}

export const elementIdCollection = {
    inputId: 'inputId',
    submitId: 'submitId'
}

export class List extends React.PureComponent<List.Props> {

    constructor(props: List.Props) {
        super(props);
    }

    onScroll = (event) => {
        const element = event.target;
        if (Math.round((element.scrollHeight - element.scrollTop) * 0.9) < element.clientHeight) {
            this.props.loadMoreRecipes();
        }
    }

    render = () => (
        <Container
            onScroll={this.onScroll}
        >
            {
                this.props.recipeList.map((recipe, i) => (
                    <RecipeItem
                        key={i}
                        recipe={recipe}
                        viewDetailClick={() => this.props.onViewDetailClick(recipe)}
                    />
                ))
            }
        </Container>
    )
};

export default List;