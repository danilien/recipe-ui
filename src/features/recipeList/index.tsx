import * as React from 'react';
import Header from './header';
import List from './list';
import { RootState } from 'src/state/types';
import { connect } from 'react-redux';
import { requestRecipes } from './state/actions';
import { ServiceRequestStatus, CONNECTION_ERROR } from 'src/common/constants';
import Loader from 'src/components/Loader';
import { useAlert } from 'react-alert'

export namespace RecipeList {
    export interface State {
        currentBatch: number;
        searchText: string;
        historySearchList: string[];
    }

    export interface Props {
        //Props from Redux
        requestRecipes: (searchText: string, loadMore: boolean) => void;
        requestStatus: ServiceRequestStatus;
        errorMessage: string | null;
        recipeList: Recipe[];
    }
}

export class RecipeList extends React.Component<RecipeList.Props, RecipeList.State> {

    constructor(props: RecipeList.Props) {
        super(props);
        this.state = {
            currentBatch: 0,
            searchText: '',
            historySearchList: []
        }
    }

    componentDidMount = () => {
        this.props.requestRecipes(this.state.searchText, true);
    }

    private onViewDetailClick = (recipe: Recipe) => {
        window.open(recipe.href);
    }

    private onChangeSearchText = (searchText: string) => {
        this.setState({
            searchText: searchText.trim()
        })
    }

    private searchRecipes = () => {
        if (this.state.searchText.length > 2) {
            const searchAlreadyUsed = this.state.historySearchList.includes(this.state.searchText) || this.state.searchText === '';
            this.props.requestRecipes(this.state.searchText, !searchAlreadyUsed);
            if (!searchAlreadyUsed) {
                this.setState((prevState: RecipeList.State) => {
                    const searchTextHistory = [this.state.searchText, ...prevState.historySearchList];
                    return {
                        historySearchList: searchTextHistory
                    }
                })
            }
        }
    }

    private searchAgain = (index: number) => {
        this.setState({
            searchText: this.state.historySearchList[index]
        },
            this.searchRecipes
        )
    }

    private deleteSearch = (index: number) => {
        this.setState((prevState: RecipeList.State) => {
            const historySearchList = [...prevState.historySearchList];
            historySearchList.splice(index, 1);
            return { historySearchList }
        })
    }

    private loadMoreRecipes = () => {
        if (this.props.requestStatus === ServiceRequestStatus.RECEIVED) {
            this.props.requestRecipes(this.state.searchText, true);
        }
    }

    public render = (): JSX.Element => {
        if (this.props.requestStatus === ServiceRequestStatus.ERROR) {
            const alert = useAlert();
            alert.error(CONNECTION_ERROR);
        }

        return (
            <>
                <Header
                    searchText={this.state.searchText}
                    onChangeSearchText={this.onChangeSearchText}
                    forceSearch={this.searchRecipes}
                    historySearchList={this.state.historySearchList.slice(0, 5)}
                    searchAgain={this.searchAgain}
                    deleteSearch={this.deleteSearch}
                />
                <List
                    recipeList={this.props.recipeList}
                    onViewDetailClick={this.onViewDetailClick}
                    loadMoreRecipes={this.loadMoreRecipes}
                />
                {
                    (this.props.requestStatus === ServiceRequestStatus.PENDING) &&
                    <Loader />
                }
            </>
        );
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        requestStatus: state.recipeListState.requestStatus,
        errorMessage: state.recipeListState.errorMessage,
        recipeList: state.recipeListState.recipeList
    };
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        requestRecipes: (searchText: string, loadMore: boolean) => { dispatch(requestRecipes(searchText, loadMore)); }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipeList);