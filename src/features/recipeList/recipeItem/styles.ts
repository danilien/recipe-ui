import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    width: calc(50% - 80px);
    box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
    border-radius: 2px;
    margin: 20px 40px 20px 40px
    @media (max-width: 768px) {
        width: calc(100% - 80px);
    }
    cursor: pointer;
    &: hover {
        background-color: #0070c9;
    }
`

export const ImageContainer = styled.div`
    display: flex;
    justify-content: center;
    width: 35%;
    height: auto;
    position: relative;
    overflow: hidden;
    margin: 30px;
    border-radius: 2px;
`

export const Image = styled.img`
    object-fit: contain;
    width: 100%;
    height: auto;
`

export const Label = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 120%;
    height: 3vw;
    color: white;
    background-color: blue;
    position: absolute;
    transform: rotateY(0deg) rotate(35deg);
    left: 15%;
    top: 15%;
    font-size: 1vw;
    @media (max-width: 768px) {
        font-size: 2vw;
        height: 5vw;
    }
`

export const Description = styled.ul`
    display: block;
    text-align: center;
    font-family: Averta,avenir w02,Avenir,Helvetica,Arial,sans-serif;
    list-style: none;
    padding-left: 0px;
`

export const ListElement = styled.li`
    @media (max-width: 768px) {
        font-size: 14px;
    }
    font-size: 20px;
`

export const Title = styled(ListElement)`
    font-weight: bold;
`

export const MainTitle = styled(Title)`
    font-size: 24px;
    color: tomato;
    @media (max-width: 768px) {
        font-size: 18px;
    }
`