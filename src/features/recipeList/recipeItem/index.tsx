import * as React from 'react';
import { Container, Image, Label, ImageContainer, Description, MainTitle, Title, ListElement } from './styles';

export namespace RecipeItem {
    export interface Props {
        viewDetailClick: () => void;
        recipe: Recipe;
    }
}

export const elementIdCollection = {
    containerId: 'containerId',
    descriptionId: 'descriptionId',
    ingredientId: (i: number) => `ingredientId${i}`
}

class RecipeItem extends React.PureComponent<RecipeItem.Props> {
    render = () => (
        <Container id={elementIdCollection.containerId} onClick={this.props.viewDetailClick}>
            <ImageContainer>
                {
                    this.props.recipe.containsLactose &&
                    <Label>Contains Lactose</Label>
                }
                <Image src={this.props.recipe.thumbnail} onError={(e: any) => { e.target.onError = null; e.target.src = "http://izuum.com/noimage.jpg" }} />
            </ImageContainer>
            <Description id={elementIdCollection.descriptionId}>
                <MainTitle>{this.props.recipe.title}</MainTitle>
                <Title>Ingredients</Title>
                {
                    this.props.recipe.ingredients.map((ingredient, i) =>
                        <ListElement id={elementIdCollection.ingredientId(i)} key={i}>{ingredient}</ListElement>
                    )
                }
            </Description>
        </Container>
    )
};

export default RecipeItem;