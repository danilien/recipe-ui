import { Action } from 'redux';

export const REQUEST_RECIPES = 'REQUEST_RECIPES';
export const REQUEST_RECIPES_SUCCESS = 'REQUEST_RECIPES_SUCCESS';
export const REQUEST_RECIPES_FAILURE = 'REQUEST_RECIPES_FAILURE';

export interface ActionRequestRecipes extends Action {
    searchText: string;
    loadMore: boolean;
}

export interface ActionRequestRecipesSuccess extends Action {
    recipeList: Recipe[];
}

export interface ActionRequestRecipesFail extends Action {
    error: string
}


export const requestRecipes = (searchText: string, loadMore: boolean): ActionRequestRecipes => ({
    type: REQUEST_RECIPES,
    searchText,
    loadMore
});

export const requestRecipesSuccess = (recipeList: Recipe[]): ActionRequestRecipesSuccess => ({
    type: REQUEST_RECIPES_SUCCESS,
    recipeList
});

export const requestRecipesFail = (error: string): ActionRequestRecipesFail => ({
    type: REQUEST_RECIPES_FAILURE,
    error
});