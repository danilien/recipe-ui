import { put } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga/effects';
import { APICache } from 'src/common/network';
import {
    REQUEST_RECIPES,
    requestRecipesSuccess as requestRecipesSuccess,
    requestRecipesFail,
    ActionRequestRecipes
} from './actions';

function* requestRecipes(action: ActionRequestRecipes) {
    try {
        const apiCacheInstance = APICache.getInstance();
        try {
            const recipes = yield apiCacheInstance.requestRecipes('', action.searchText, action.loadMore);
            yield put(requestRecipesSuccess(recipes));
        } catch (e) {
            yield put(requestRecipesFail(e));
        }
    } catch (e) {
        yield put(requestRecipesFail('Unknown error'));
    }
}

export default function* recipeListSagas() {
    yield takeLatest(REQUEST_RECIPES, requestRecipes);
}
