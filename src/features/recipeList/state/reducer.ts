import { Reducer } from 'redux';
import { ServiceRequestStatus } from 'src/common/constants';
import {
    REQUEST_RECIPES,
    REQUEST_RECIPES_SUCCESS,
    REQUEST_RECIPES_FAILURE
} from './actions';

export interface RecipeListState {
    requestStatus: ServiceRequestStatus;
    errorMessage: string | null;
    recipeList: Recipe[];
}

export const initialState = {
    requestStatus: ServiceRequestStatus.INACTIVE,
    errorMessage: null,
    recipeList: []
};

const recipeListReducer: Reducer<RecipeListState> = (state: RecipeListState = initialState, action): RecipeListState => {
    switch (action.type) {
        case REQUEST_RECIPES: {
            return {
                ...state,
                requestStatus: ServiceRequestStatus.PENDING
            };
        }
        case REQUEST_RECIPES_SUCCESS: {
            const recipeList = action.recipeList.sort((r1: Recipe, r2: Recipe) => r1.title.localeCompare(r2.title));
            return {
                ...state,
                recipeList,
                requestStatus: ServiceRequestStatus.RECEIVED
            };
        }
        case REQUEST_RECIPES_FAILURE: {
            return {
                ...state,
                errorMessage: action.error,
                requestStatus: ServiceRequestStatus.ERROR
            };
        }
        default:
    }
    return state;
};

export default recipeListReducer;