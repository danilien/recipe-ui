import styled from "styled-components";
import { FullSizeFlexDiv } from 'src/common/styles';

export const Container = styled(FullSizeFlexDiv)`
    flex-direction: column;
    box-shadow: -1px 1px 5px rgba(175, 185, 218, 0.5);
`;