import styled from 'styled-components';
import { FlexCenter } from 'src/common/styles';

export const Container = styled.div`
    z-index: 1;
    padding: 10px;
`

export const SearchBar = styled(FlexCenter)`
    border: 2px solid #CF5C3F;
	overflow: auto;
    border-radius: 5px;
`

export const Input = styled.input`
    border: 0px;
    width: calc(100% - 80px);
    padding: 10px 10px;
`

export const Submit = styled.div`
    border: 0px;
    background: none;
    background-color: #CF5C3F;
    color: #fff;
    float: right;
    padding: 10px;
    cursor: pointer;
    width: 80px;
`

export const SearchMenuSection = styled.div`
    background: #fff;
    cursor: pointer;
    color: black;
`