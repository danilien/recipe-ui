import * as React from 'react';
import { SearchBar, Submit, Input, Container, SearchMenuSection } from './styles';
import { Collapsable } from 'src/components/Collapsable';
import SearchMenuHistory from './searchHistoryMenu';

export namespace Header {
    export interface Props {
        searchText: string;
        onChangeSearchText: (value: string) => void;
        forceSearch: () => void;
        historySearchList: string[];
        searchAgain: (index: number) => void;
        deleteSearch: (index: number) => void;
    }
}

export const elementIdCollection = {
    inputId: 'inputId',
    submitId: 'submitId'
}

export class Header extends React.PureComponent<Header.Props> {

    constructor(props: Header.Props) {
        super(props);
    }

    render = () => (
        <Container>
            <SearchBar>
                {
                    this.props.historySearchList.length ?
                        <SearchMenuSection>
                            <Collapsable>
                                <SearchMenuHistory {...this.props} />
                            </Collapsable>
                        </SearchMenuSection> : null
                }
                <Input
                    id={elementIdCollection.inputId}
                    type="text"
                    placeholder="Search for recipes.."
                    onChange={(event) => { this.props.onChangeSearchText(event.target.value) }}
                    onKeyPress={(event) => { if (event.key === 'Enter') this.props.forceSearch() }}
                    value={this.props.searchText}
                />
                <Submit
                    id={elementIdCollection.submitId}
                    onClick={() => this.props.forceSearch()}
                >
                    Search
                </Submit>
            </SearchBar>
        </Container>
    )
};

export default Header;