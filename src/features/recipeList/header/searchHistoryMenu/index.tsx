import * as React from 'react';
import { Container, MenuElement, Option, LimitedOption } from './styles';

export namespace SearchMenuHistory {
    export interface Props {
        historySearchList: string[];
        searchAgain: (index: number) => void;
        deleteSearch: (index: number) => void;
    }
}

export const elementIdCollection = {
    searchText: (i: number) => `searchText${i}`,
    delete: (i: number) => `delete${i}`,
}

class SearchHistoryMenu extends React.PureComponent<SearchMenuHistory.Props> {
    render = () => (
        <Container>
            {
                this.props.historySearchList.map((searchText, i) =>
                    <MenuElement key={i} id={i.toString()}>
                        <LimitedOption id={elementIdCollection.searchText(i)} onClick={() => this.props.searchAgain(i)}>
                            {searchText}
                        </LimitedOption>
                        <Option id={elementIdCollection.delete(i)} onClick={() => this.props.deleteSearch(i)}>
                            delete
                        </Option>
                    </MenuElement>
                )
            }
        </Container>
    )
};

export default SearchHistoryMenu;