import styled from "styled-components";
import { FullSizeFlexDiv, FlexDiv, FlexCenter } from 'src/common/styles';

export const Container = styled(FullSizeFlexDiv)`
    flex-direction: column;
    box-shadow: -1px 1px 5px rgba(175, 185, 218, 0.5);
`;

export const MenuElement = styled(FullSizeFlexDiv)`
    justify-content: space-between;
    padding: 10px;
    text-decoration: none;
`;

export const Option = styled.p`
    margin: 0px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    cursor: pointer;
    &:hover {
        color: blue;
    }
`;

export const LimitedOption = styled(Option)`
    max-width: 70%;
`;