
import styled from 'styled-components';

export const AppContainer = styled.div`
    display: flex;
    flex-flow:row;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    width: 100%;
    height: 100%;
    overflow: hidden;
`
export const AppContent = styled.div`
    display:flex;
    flex-direction: column;
    height:100vh;
    width:100%;
`