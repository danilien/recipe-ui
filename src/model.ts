declare interface Recipe {
    title: string;
    href: string;
    ingredients: string[];
    thumbnail: string;
    containsLactose: boolean;
}