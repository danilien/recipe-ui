# Recipes UI

## Setup & run

- Execute: `npm i` and `npm start`

## Libraries used
- Typescript for data typing.
- React + Redux + Router: for managing the state app and routing to the only '/recipeList' path
- styled-components library: for css management. I prefer to use a higher level tool for managing the style over pure css classes.
- jest + enzyme for testing. I haven't tried any other framework options. But they work pretty well together plus is being adopted accross all the community

## Missing/improve stuff
- I only implemented the seach by recipe name. As there are two search criteria, by ingredientes and by name, it only made sense to implement both only having different search boxes. But I run out of time in order to implement the second search box.
- I used the initial http://www.recipepuppy.com/about/api/ instead the https://badi-recipes.now.sh/api. That's becuase I run into the XCORS issue when making HTTP requests on both APIs due to the 'Access-Control-Allow-Origin' header missing. There's probably another workaround, but most of the ones I found involved to change the headers returned from the API server. I found a workaround using JSONP to call the API but that option was only active on http://www.recipepuppy.com/about/api/. That's why I kept using it.
- Error handling on API connection errors: there is an alert rising when an error occurs, but is pretty basic and has almost no testing.
- Network cache improve: is a basic cache implementation for the API requests. Should be improved: invalidate cache by time, dependency injection for testing.
- Network cache tests: they are totally missing :(.
- React Hooks: I am still not used to them. With some more time I would have played a little bit with them.